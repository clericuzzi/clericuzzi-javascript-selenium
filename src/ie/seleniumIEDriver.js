const webDriver = require('selenium-webdriver');
const SeleniumDriver = require('../base/seleniumDriver');

module.exports = class SeleniumChromeDriver extends SeleniumDriver
{
    constructor(capabilities)
    {
        if (!capabilities)
            capabilities = webDriver.Capabilities.ie();

        super(capabilities);
    }

    async fill(element, text)
    {
        try
        {
            if (element)
                await element.sendKeys(text);
        }
        catch (err)
        {
            console.log(`could not fill the element`, err);
            throw err;
        }
    }
}