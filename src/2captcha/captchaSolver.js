const Driver = require('../base/seleniumDriver');

const fs = require('fs');
const httpManager = require('clericuzzi-javascript-http/dist/business/managers/http.manager');
const captchaSolver = require('2captcha-node').default;

let _key = null;
let _tries = 60;
let _solver = null;
/**
 * simple captcha solver method
 *
 * @param {*} imagepath the path to where the captcha image will be saved (and later uploaded to the server)
 * @returns TRUE
 */
module.exports.simpleCaptchaSolve = async function (imagepath)
{
    if (!fs.existsSync(imagepath))
        throw Error(`the file '${imagepath}' does not exist`);

    try
    {
        const base64 = Buffer.from(fs.readFileSync(imagepath)).toString('base64');
        const options = {
            image: base64,
            maxAttempts: _tries
        };

        const solvedCaptcha = await _solver.solve(options);
        return solvedCaptcha;
    }
    catch (err)
    {
        throw Error(`the captcha could not be broken: `, err)
    }
}

/**
 *
 *
 * @param {Driver} driver web driver
 * @param {string} url the current url
 * @param {string} gRecaptchaPath recaptcha key element's path
 * @param {string} gResponsePath recaptcha response element's path
 * @param {string} captchaSubmitButton recaptcha submit button's path
 * @returns the captcha returned
 */
module.exports.recaptchaSolve = async function (driver, url, gRecaptchaPath, gResponsePath, captchaSubmitButton)
{
    const recaptchaUrl = `https://2captcha.com/in.php`;
    const responseCaptchaUrl = `http://2captcha.com/res.php`;
    try
    {
        const element = await driver.getElement(gRecaptchaPath);

        const siteKey = await driver.getAttribute(element, `data-sitekey`);
        const getInitialRequestUrl = `${recaptchaUrl}?key=${_key}&method=userrecaptcha&googlekey=${siteKey}&pageurl=${url}`;
        console.log(getInitialRequestUrl);

        const initialRequest = await httpManager.get(null, getInitialRequestUrl);
        const requestId = recaptchaValidateInitialRequest(initialRequest);
        if (requestId)
        {
            let tries = 0;
            const getRequestUrl = `${responseCaptchaUrl}?key=${_key}&action=get&id=${requestId}`;
            console.log(getRequestUrl);
            await driver.sleepRandom(16000, 20000);
            let captchaValue = null;
            while (!captchaValue && tries < _tries)
            {
                captchaValue = await httpManager.get(null, getRequestUrl);
                captchaValue = recaptchaValidateCaptchaRequest(captchaValue);
                if (captchaValue)
                    break;

                await driver.sleepRandom(3000, 7000);
                tries++;
            }

            await driver.setContent(gResponsePath, captchaValue);
            await driver.sleepRandom(3000, 7000);
            console.log(`captcha solved!`);
            await driver.click(captchaSubmitButton);

            return captchaValue;
        }
    }
    catch (err)
    {
        console.log(err);
        throw Error(`the captcha could not be broken: `, err)
    }
};

const recaptchaValidateInitialRequest = initialRequest =>
{
    initialRequest = initialRequest.toLocaleUpperCase();
    if (initialRequest.startsWith(`OK|`))
        return initialRequest.replace(`OK|`, ``);
    else
        throw new Error(`Captcha solver recaptchaValidateInitialRequest: request was not successfull (${initialRequest})`);
};
const recaptchaValidateCaptchaRequest = captchaRequestValue =>
{
    captchaRequestValue = captchaRequestValue.toLocaleUpperCase();
    if (captchaRequestValue.startsWith(`OK|`))
        return captchaRequestValue.substring(3);
    else
    {
        console.log(`captcha not ready: `, captchaRequestValue);
        return null;
    }
};

module.exports.recaptchaClick = async (driver, iframe, button, timeout = 15000) =>
{
    await driver.wait(iframe, timeout);
    await driver.switchToFrame(iframe);
    await driver.wait(button, 5000);
    await driver.click(button);
    await driver.switchToRoot();
}

module.exports.initSolver = function (key, tries = 16)
{
    _key = key;
    _tries = tries;
    _solver = captchaSolver(key);
}