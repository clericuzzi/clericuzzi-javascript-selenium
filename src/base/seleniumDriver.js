const WebElement = require('selenium-webdriver').WebElement;
const { Builder } = require('selenium-webdriver');

const fs = require('fs');

const sharp = require('sharp');

const By = require('selenium-webdriver').By;
const Key = require('selenium-webdriver').Key;
const until = require('selenium-webdriver').until;
const selenium = require('selenium-webdriver');

const _defaultScreenShotFileName = 'current-screen-shot.png';

sharp.cache(false);
module.exports = class SeleniumDriver
{
    /**
     *Creates an instance of SeleniumDriver.
     * @param {Builder} builder the actual webdriver's builder
     */
    constructor(builder)
    {
        this.webDriver = builder.build();
    }

    async moveAndClick(settings)
    {
        try
        {
            await this.webDriver.actions()
                .move(settings)
                .click()
                .perform();
        }
        catch (err)
        {
            err.customMessage = `could not moveAndClick`;
            throw err;
        }
    }

    //screenshot section
    async screenshot(imagePath)
    {
        try
        {
            const image = await this.webDriver.takeScreenshot();
            fs.writeFileSync(imagePath, image, `base64`);

            return image;
        }
        catch (err)
        {
            err.customMessage = `could not screenshot`;
            throw err;
        }
    }
    async screenshotCropById(id, imagePath)
    {
        try
        {
            const element = await this.getElementById(id)
            if (element)
                await this.screenshotCropByElement(element, imagePath);
            else
                throw Error(`screenshotCropById: the element '${id}' could not be found`);
        }
        catch (e)
        {
            err.customMessage = `could not screenshotCropById`;
            throw err;
        }
    }
    async screenshotCropByXPath(xpath, imagePath)
    {
        try
        {
            const element = await this.getElement(xpath);
            if (element)
                await this.screenshotCropByElement(element, imagePath);
            else
                throw Error(`screenshotCropByXPath: the element '${xpath}' could not be found`);
        }
        catch (err)
        {
            err.customMessage = `could not screenshotCropByXPath`;
            throw err;
        }
    }
    async screenshotCropByElement(element, imagePath)
    {
        try
        {
            if (!element)
                throw Error(`screenshotCropByElement: the element provided cannot be null`);
            const elementRect = await element.getRect();
            if (elementRect)
            {
                let top = Number.parseInt(elementRect.y);
                let left = Number.parseInt(elementRect.x);
                const width = Number.parseInt(elementRect.width);
                const height = Number.parseInt(elementRect.height);

                const XOffset = await this.webDriver.executeScript(`return window.pagexOffset;`);
                const yOffset = await this.webDriver.executeScript(`return window.pageYOffset;`);
                top -= yOffset;
                left -= XOffset;
                await this.screenshot(_defaultScreenShotFileName);
                await sharp(_defaultScreenShotFileName).extract({ height, width, left, top }).toFile(imagePath);
                if (fs.existsSync(_defaultScreenShotFileName))
                    fs.unlinkSync(_defaultScreenShotFileName);
            }
        }
        catch (err)
        {
            err.customMessage = `could not screenshotCropByElement`;
            throw err;
        }
    }
    //screenshot section END

    async kill()
    {
        try
        {
            if (this.webDriver)
                await this.webDriver.quit();
            this.webDriver = null;
        }
        catch (err)
        {
            err.customMessage = `could not kill`;
            throw err;
        }
    }
    maximize()
    {
        try
        {
            this.webDriver.manage().window().maximize();
        }
        catch (err)
        {
            err.customMessage = `could not maximize`;
            throw err;
        }
    }
    minimize()
    {
        try
        {
            this.webDriver.manage().window().minimize();
        }
        catch (err)
        {
            err.customMessage = `could not minimize`;
            throw err;
        }
    }

    // scripts section
    async runScript(script, args)
    {
        if (!args)
            await this.webDriver.executeScript(script);
        else
            await this.webDriver.executeScript(script, args);
    }
    async runScriptAsync(script, args)
    {
        if (!args)
            await this.webDriver.executeAsyncScript(script);
        else
            await this.webDriver.executeAsyncScript(script, args);
    }
    // scripts section END

    // SWITCH section
    async switchToRoot()
    {
        try
        {
            await this.webDriver.switchTo().defaultContent();
        }
        catch (err)
        {
            err.customMessage = `could not switchToRoot`;
            throw err;
        }
    }
    async switchToFrame(frame)
    {
        try
        {
            if (typeof frame === `string`)
                await this.webDriver.switchTo().frame(await this.getElement(frame));
            else
                await this.webDriver.switchTo().frame(frame);
        }
        catch (err)
        {
            err.customMessage = `could not switchToFrame`;
            throw err;
        }
    }
    // SWITCH section END

    // wait section
    async wait(xpath, timeout = 10000, throwsError = true)
    {
        try
        {
            await this.webDriver.wait(until.elementLocated(By.xpath(xpath)), timeout);
            return true;
        }
        catch (err)
        {
            if (throwsError)
                throw err;
            else
                return false;
        }
    }
    async waitById(id, timeout = 10000, throwsError = true)
    {
        try
        {
            await this.webDriver.wait(until.elementLocated(By.id(id)), timeout);
            return true;
        }
        catch (err)
        {
            if (throwsError)
                throw err;
            else
                return false;
        }
    }
    async waitAlert(timeout = 10000, throwsError = true)
    {
        try
        {
            return await this.webDriver.wait(until.alertIsPresent(), timeout);
        }
        catch (err)
        {
            if (throwsError)
                throw err;
            else
                return false;
        }
    }
    async waitAlertMessage(message, timeout = 10000, throwsError = true)
    {
        try
        {
            const alert = await this.webDriver.wait(until.alertIsPresent(), timeout);
            let alertText = await this.webDriver.switchTo().alert().getText();
            message = message.toLocaleLowerCase();
            alertText = alertText.toLocaleLowerCase();

            return alertText.indexOf(message) >= 0;
        }
        catch (err)
        {
            if (throwsError)
                throw err;
            else
                return false;
        }
    }
    async waitUntilExists(xpath, timeout = 10000, throwsError = true)
    {
        try
        {
            await this.waitTrueEvaluation(async () => await this.exists(xpath), timeout, 750, throwsError);

            return this.exists(xpath);
        }
        catch (err)
        {
            throw new Error(`the item '${xpath}' could not be found...`);
        }
    }
    async waitUntilNotExists(xpath, timeout = 10000, throwsError = true)
    {
        try
        {
            await this.waitTrueEvaluation(async () => !(await this.exists(xpath)), timeout, 750, throwsError);

            return !this.exists(xpath);
        }
        catch (err)
        {
            throw new Error(`the item '${xpath}' never went away...`);
        }
    }
    async waitUntilExistsAndIsDisplayed(xpath, timeout = 10000, throwsError = true)
    {
        try
        {
            await this.waitTrueEvaluation(async () => await this.exists(xpath) && await this.isDisplayed(xpath), timeout, 750, throwsError);

            return this.exists(xpath);
        }
        catch (err)
        {
            throw new Error(`the item '${xpath}' could not be found...`);
        }
    }
    async waitUntilNotExistsOrNotDisplayed(xpath, timeout = 10000, throwsError = true)
    {
        try
        {
            await this.waitTrueEvaluation(async () => !(await this.exists(xpath)) || !(await this.isDisplayed(xpath)), timeout, 750, throwsError);

            return !this.exists(xpath);
        }
        catch (err)
        {
            throw new Error(`the item '${xpath}' never went away...`);
        }
    }
    async waitTrueEvaluation(action, timeout = 10000, polling = 750, throwsError = true, instance = null, ...params) 
    {
        try
        {
            const start = new Date();
            await this.sleep(polling);
            let timeElapsed = new Date() - start;
            while (timeElapsed < timeout)
            {
                let actionValue = null;
                if (!instance && !params)
                    actionValue = await action.apply();
                else
                    actionValue = await action.apply(instance, params);
                if (actionValue)
                {
                    await this.sleep(polling);
                    return;
                }

                await this.sleep(polling);
                timeElapsed = new Date() - start;
            }

            if (throwsError)
                throw new Error(`SeleniumDriver waitTrueEvaluation: timeout reached`);
        }
        catch (err)
        {
            if (throwsError)
                throw err;
            else
                return false;
        }
    };
    // wait section END

    async elementByTag(tag)
    {
        try
        {
            return await this.webDriver.findElement(By.tagName(tag));
        }
        catch (err)
        {
            err.customMessage = `could not elementByTag`;
            throw err;
        }
    }
    async elementsByTag(tag)
    {
        try
        {
            return await this.webDriver.findElements(By.tagName(tag));
        }
        catch (err)
        {
            err.customMessage = `could not elementsByTag`;
            throw err;
        }
    }

    // alert section
    async alertHas()
    {
        try
        {
            await this.sleep(750);
            const alertText = await this.webDriver.switchTo().alert().getText();

            return alertText !== null && alertText !== undefined && alertText.length > 0;
        }
        catch (err)
        {
            return false;
        }
    }
    async alertHasMessage(message)
    {
        try
        {
            await this.sleep(750);
            const alertText = await this.webDriver.switchTo().alert().getText();
            if (alertText !== null && alertText !== undefined && alertText.length > 0)
            {
                message = message.toLocaleLowerCase();
                alertText = alertText.toLocaleLowerCase();

                return alertText.indexOf(message) >= 0;
            }
            else
                return false;
        }
        catch (err)
        {
            err.customMessage = `could not alertHasMessage`;
            throw err;
        }
    }
    async alertAccept()
    {
        try
        {
            await this.sleep(750);
            await this.webDriver.switchTo().alert().accept();
        }
        catch (err)
        {
            err.customMessage = `could not alertAccept`;
            throw err;
        }
    }
    async alertDismiss()
    {
        try
        {
            await this.sleep(750);
            await this.webDriver.switchTo().alert().dismiss();
        }
        catch (err)
        {
            err.customMessage = `could not alertDismiss`;
            throw err;
        }
    }
    // alert section end

    async url()
    {
        try
        {
            return await this.webDriver.getCurrentUrl();
        }
        catch (err)
        {
            err.customMessage = `could get current url`;
            throw err;
        }
    }

    async sleep(sleep)
    {
        try
        {
            await this.webDriver.sleep(sleep);
        }
        catch (err)
        {
            err.customMessage = `could not sleep`;
            throw err;
        }
    }
    async sleepRandom(rangeMin, rangeMax)
    {
        try
        {
            const time = Math.floor(Math.random() * (+rangeMax - +rangeMin)) + +rangeMin;
            console.log(`random sleep:`, time);

            await this.webDriver.sleep(time);
        }
        catch (err)
        {
            err.customMessage = `could not sleep`;
            throw err;
        }
    }

    async back()
    {
        try
        {
            await this.webDriver.navigate().back();
        }
        catch (err)
        {
            err.customMessage = `could not go back`;
            throw err;
        }
    }
    async forward()
    {
        try
        {
            await this.webDriver.navigate().forward();
        }
        catch (err)
        {
            err.customMessage = `could not go forward`;
            throw err;
        }
    }
    async refresh()
    {
        try
        {
            await this.webDriver.navigate().refresh();
        }
        catch (err)
        {
            err.customMessage = `could not refresh`;
            throw err;
        }
    }
    async navigate(url)
    {
        try
        {
            await this.webDriver.get(url);
        }
        catch (err)
        {
            err.customMessage = `could not navigate`;
            throw err;
        }
    }

    /**
     * clicks a WebElement
     *
     * @param {string | WebElement} xpathOrElement the xpath that points to the desired element or the desired element itself
     */
    async click(xpathOrElement)
    {
        try
        {
            let element = null;
            if (typeof xpathOrElement === `string`)
                element = await this.getElement(xpathOrElement);
            else
                element = xpathOrElement;
            if (!element)
                throw Error(`click must receive a valid element`);

            await element.click();
        }
        catch (err)
        {
            err.customMessage = `could not click`;
            throw err;
        }
    }
    /**
     * checks if a WebElement is enable
     *
     * @param {string | WebElement} xpathOrElement the xpath that points to the desired element or the desired element itself
     * @returns {string} TRUE if it is enabled,, false otherwise
     */
    async isEnabled(xpathOrElement)
    {
        try
        {
            let element = null;
            if (typeof xpathOrElement === `string`)
                element = await this.getElement(xpathOrElement);
            else
                element = xpathOrElement;
            if (!element)
                throw Error(`isEnabled must receive a valid element`);

            return await element.isEnabled();
        }
        catch (error)
        {
            err.customMessage = `could not isEnabled`;
            throw err;
        }
    }

    async getChildByTagAndAttribute(xpathOrElement, tag, attribute, attributeValue)
    {

        try
        {
            let element = null;
            if (typeof xpathOrElement === `string`)
                element = await this.getElement(xpathOrElement);
            else
                element = xpathOrElement;
            if (!element)
                throw Error(`getChildByTagAndAttribute must receive a valid element`);

            const children = await element.findElements(By.tagName(tag));
            for (const child of children)
            {
                const childAttr = await child.getAttribute(attribute);
                if (childAttr === attributeValue)
                    return child;
            }

            return null;
        }
        catch (err)
        {
            err.customMessage = `could not getChildByTagAndAttribute`;
            throw err;
        }
    }

    /**
     * gets an attribute from a WebElement
     *
     * @param {string | WebElement} xpathOrElement the xpath that points to the desired element or the desired element itself
     * @param {string} attribute the desired attribute
     * @returns {string} the desired attribute's value
     */
    async getAttribute(xpathOrElement, attribute)
    {
        try
        {
            let element = null;
            if (typeof xpathOrElement === `string`)
                element = await this.getElement(xpathOrElement);
            else
                element = xpathOrElement;

            return await element.getAttribute(attribute);
        }
        catch (err)
        {
            err.customMessage = `could not getAttribute from '${xpathOrElement}'`;
            throw err;
        }
    }
    async setAttribute(xpathOrElement, attribute, value)
    {
        try
        {
            let webElement = null;
            if (typeof xpathOrElement === `string`)
                webElement = await this.getElement(xpathOrElement);
            else
                webElement = xpathOrElement;

            this.webDriver.executeScript(`arguments[0].setAttribute(arguments[1], arguments[2])`, webElement, attribute, value);
        }
        catch (err)
        {
            err.customMessage = `could not setAttribute for '${xpathOrElement}'`;
            throw err;
        }
    }

    /**
     * gets a list of children filtered by the given tag
     *
     * @param {string | WebElement} xpathOrElement the xpath that points to the desired element or the desired element itself
     * @param {string} tag the desired type of elements
     * @returns {WebElement[]} the itens found under the given element that matches the given tag
     */
    async getElementsByTag(xpathOrElement, tag)
    {
        try
        {
            let element = null;
            if (typeof xpathOrElement === `string`)
                element = await this.getElement(xpathOrElement);
            else
                element = xpathOrElement;
            if (!element)
                throw Error(`getElementsByTag must receive a valid element`);

            return await element.findElements(By.tagName(tag));
        }
        catch (err)
        {
            err.customMessage = `could not getElementsByTag`;
            throw err;
        }
    }

    /**
     * returns the element text
     *
     * @param {string | WebElement} xpathOrElement the xpath that points to the desired element or the desired element itself
     * @returns {string} the corresponding WebElement content
     */
    async getText(xpathOrElement)
    {
        let element = null;
        try
        {
            if (typeof xpathOrElement === `string`)
                element = await this.getElement(xpathOrElement);
            else
                element = xpathOrElement;

            let text = await element.getText();
            if (text === null || text === undefined || text === ``)
                text = await element.getAttribute(`value`);

            if (text && text.trim)
                text = text.trim();
            return text;
        }
        catch (err)
        {
            err.customMessage = `could not getText`;
            throw err;
        }
    }
    /**
     * returns the element pointed by the givenPath
     *
     * @param {string | WebElement} xpathOrElement the xpath that points to the desired element or the desired element itself
     * @returns the corresponding WebElement bounds
     */
    async getRect(xpathOrElement)
    {
        try
        {
            let element = await this.getElement(xpathOrElement);
            if (typeof xpathOrElement === `string`)
                element = await this.getElement(xpathOrElement);
            else
                element = xpathOrElement;
            if (!element)
                throw Error(`getRect must receive a valid element`);

            return await element.getRect();
        }
        catch (err)
        {
            err.customMessage = `could not getRect`;
            throw err;
        }
    }

    /**
     * returns the element pointed by the givenPath
     *
     * @param {string} xpath the xpath that points to the desired element
     * @returns {WebElement} the corresponding WebElement, if it exists
     */
    async getElement(xpath)
    {
        try
        {
            await this.wait(xpath, 5000);
            return await this.webDriver.findElement(By.xpath(xpath));
        }
        catch (err)
        {
            err.customMessage = `could not getElement`;
            throw err;
        }
    }

    /**
     * evaluates if an element exists
     *
     * @param {string} xpath the xpath that points to the desired element
     * @returns {boolean} TRUE if the element exists, false otherwise
     */
    async exists(xpath)
    {
        try
        {
            const element = await this.webDriver.findElement(By.xpath(xpath));
            return element !== null;
        }
        catch
        {
            return false;
        }
    }
    /**
     * evaluates if an element is displayed wherever on the page
     *
     * @param {string} xpath the xpath that points to the desired element
     * @returns {boolean} TRUE if the element is being displayed, false otherwise
     */
    async isDisplayed(xpathOrElement)
    {
        let element = null
        try
        {
            if (typeof xpathOrElement === `string`)
                element = await this.getElement(xpathOrElement);
            else
                element = xpathOrElement;

            return await element.isDisplayed();
        }
        catch
        {
            return false;
        }
    }
    async isNotDisplayed(xpathOrElement)
    {
        let element = null
        try
        {
            if (typeof xpathOrElement === `string`)
                element = await this.getElement(xpathOrElement);
            else
                element = xpathOrElement;

            return !(await element.isDisplayed());
        }
        catch
        {
            return null;
        }
    }

    async sendTab(xpathOrElement, throwsError = false)
    {
        try
        {
            let element = null;
            if (typeof xpathOrElement === `string`)
                element = await this.getElement(xpathOrElement);
            else
                element = xpathOrElement;
            if (!element)
                throw Error(`getElementText must receive a valid element`);

            if (element)
                await element.sendKeys(Key.TAB);
        }
        catch (err)
        {
            err.customMessage = `could not sendElementTab to '${xpathOrElement}'`;
            if (throwsError)
                throw err;
        }
    }
    async sendEnter(xpathOrElement, throwsError = false)
    {
        try
        {
            let element = null;
            if (typeof xpathOrElement === `string`)
                element = await this.getElement(xpathOrElement);
            else
                element = xpathOrElement;
            if (!element)
                throw Error(`getElementText must receive a valid element`);

            if (element)
                await element.sendKeys(Key.ENTER);
        }
        catch (err)
        {
            err.customMessage = `could not sendElementEnter to '${xpathOrElement}'`;
            if (throwsError)
                throw err;
        }
    }

    async canFill(xpathOrElement)
    {
        try
        {
            let element = null;
            if (typeof xpathOrElement === `string`)
                element = await this.getElement(xpathOrElement);
            else
                element = xpathOrElement;

            const currentValue = await this.getText(xpathOrElement);
            await this.fill(element, currentValue);

            return true;
        }
        catch (err)
        {
            console.log(`err`);
            return false;
        }
    }
    async fillById(id, text)
    {
        try
        {
            const element = await this.getElementById(id);
            if (element)
                await this.fill(element, text);
        }
        catch (err)
        {
            err.customMessage = `could not fillElementById`;
            throw err;
        }
    }
    async getImmediateChildren(xpathOrElement)
    {
        try
        {
            let element = null;
            if (typeof xpathOrElement === `string`)
                element = await this.getElement(xpathOrElement);
            else
                element = xpathOrElement;

            element = await this.getElement(xpath);
            return await element.findElements(By.xpath("*"));
        }
        catch (err)
        {
            err.customMessage = `could not getImmediateChildren`;
            throw err;
        }
    }
    async getImmediateChildrenByTag(xpath, tag)
    {
        try
        {
            const element = await this.getElement(xpath);
            return await this.getElementImmediateChildrenByTag(element, tag);
        }
        catch (err)
        {
            err.customMessage = `could not getImmediateChildrenByTag`;
            throw err;
        }
    }
    /**
     * returns the element
     *
     * @param {string} id the element's id
     * @returns {WebElement} the element that matches the given id
     */
    async getElementById(id)
    {
        try
        {
            await this.waitById(id, 5000);
            return await this.webDriver.findElement(By.id(id));
        }
        catch (err)
        {
            err.customMessage = `could not getElementById`;
            throw err;
        }
    }

    /**
     * fills the component's value
     *
     * @param {string | WebElement} xpathOrElement the targeted element
     * @param {string} text the new text value 
     */
    async fill(xpathOrElement, text)
    {
        try
        {
            let element = null;
            if (typeof xpathOrElement === `string`)
                element = await this.getElement(xpathOrElement);
            else
                element = xpathOrElement;

            if (element)
            {
                if (element.getRect)
                {
                    const rect = await element.getRect();
                    if (rect)
                        this.webDriver.executeScript(`window.scrollTo(arguments[0], arguments[1])`, 0, rect.y - 200);
                }

                if (element)
                {
                    await element.clear();
                    await element.sendKeys(text);
                }
            }
        }
        catch (err)
        {
            err.customMessage = `could not fill the element`;
            throw err;
        }
    }
    /**
     * fills the component's value by javascript
     *
     * @param {string | WebElement} xpathOrElement the targeted element
     * @param {string} text the new text value 
     */
    async fillJs(xpathOrElement, text)
    {
        try
        {
            let element = null;
            if (typeof xpathOrElement === `string`)
                element = await this.getElement(xpathOrElement);
            else
                element = xpathOrElement;

            if (element)
            {
                const rect = await element.getRect();
                this.webDriver.executeScript(`window.scrollTo(arguments[0], arguments[1])`, 0, rect.y - 200);
                this.webDriver.executeScript(`arguments[0].setAttribute(arguments[1], arguments[2])`, element, `value`, text);
            }
        }
        catch (err)
        {
            err.customMessage = `could not fill the element`;
            throw err;
        }
    }


    /**
     * gets the tag for a given component
     *
     * @param {string | WebElement} xpathOrElement the targeted element
     * @returns {string} the component's tag 
     */
    async getTag(xpathOrElement)
    {
        try
        {
            let element = null;
            if (typeof xpathOrElement === `string`)
                element = await this.getElement(xpathOrElement);
            else
                element = xpathOrElement;
            if (!element)
                throw Error(`getElementTag must receive a valid element`);

            return await await element.getTagName();
        }
        catch (err)
        {
            err.customMessage = `could not getElementTag`;
            throw err;
        }
    }

    /**
     * gets the inner html for a given component
     *
     * @param {string | WebElement} xpathOrElement the targeted element
     * @returns {string} the component's inner html text 
     */
    async getContent(xpathOrElement)
    {
        try
        {
            let element = null;
            if (typeof xpathOrElement === `string`)
                element = await this.getElement(xpathOrElement);
            else
                element = xpathOrElement;
            if (!element)
                throw Error(`getElementContent must receive a valid element`);

            return await element.getAttribute('innerHTML');
        }
        catch (err)
        {
            err.customMessage = `could not getElementContent`;
            throw err;
        }
    }
    async setContent(xpathOrElement, value)
    {
        try
        {
            let element = null;
            if (typeof xpathOrElement === `string`)
                element = await this.getElement(xpathOrElement);
            else
                element = xpathOrElement;

            this.webDriver.executeScript(`arguments[0].innerHTML = arguments[1]`, element, value);
        }
        catch (err)
        {
            err.customMessage = `could not setAttribute for '${xpathOrElement}'`;
            throw err;
        }
    }

    async getChildById(xpathOrElement, id)
    {
        try
        {
            let element = null;
            if (typeof xpathOrElement === `string`)
                element = await this.getElement(xpathOrElement);
            else
                element = xpathOrElement;
            if (!element)
                throw Error(`getElementChildById must receive a valid element`);

            try
            {
                return await element.findElement(By.id(id));
            }
            catch (error)
            {
                return null;
            }
        }
        catch (err)
        {
            err.customMessage = `could not getElementChildById`;
            throw err;
        }
    }
    async getChildByTag(xpathOrElement, tag)
    {
        try
        {
            let element = null;
            if (typeof xpathOrElement === `string`)
                element = await this.getElement(xpathOrElement);
            else
                element = xpathOrElement;
            if (!element)
                throw Error(`getElementChildByTag must receive a valid element`);

            try
            {
                const child = await element.findElement(By.tagName(tag));
                return child;
            }
            catch (error)
            {
                return null;
            }
        }
        catch (err)
        {
            err.customMessage = `could not getElementChildByTag`;
            throw err;
        }
    }
    async getChildrenByTag(xpathOrElement, tag)
    {
        try
        {
            let element = null;
            if (typeof xpathOrElement === `string`)
                element = await this.getElement(xpathOrElement);
            else
                element = xpathOrElement;
            if (!element)
                throw Error(`getElementChildrenByTag must receive a valid element`);

            return await element.findElements(By.tagName(tag));
        }
        catch (err)
        {
            err.customMessage = `could not getElementChildrenByTag`;
            throw err;
        }
    }
    async getImmediateChildren(xpathOrElement)
    {
        try
        {
            let element = null;
            if (typeof xpathOrElement === `string`)
                element = await this.getElement(xpathOrElement);
            else
                element = xpathOrElement;
            if (!element)
                throw Error(`getElementImmediateChildren must receive a valid element`);

            return await element.findElements(By.xpath("*"));
        }
        catch (err)
        {
            err.customMessage = `could not getElementImmediateChildren`;
            throw err;
        }
    }
    async getImmediateChildrenByTag(xpathOrElement, tag)
    {
        try
        {
            let element = null;
            if (typeof xpathOrElement === `string`)
                element = await this.getElement(xpathOrElement);
            else
                element = xpathOrElement;
            if (!element)
                throw Error(`getElementImmediateChildrenByTag must receive a valid element`);

            const filteredChildren = [];
            let elements = await element.findElements(By.xpath("*"));
            if (elements !== null && elements !== undefined && elements.length)
            {
                for (const child of elements)
                    if (await child.getTagName() === tag)
                        filteredChildren.push(child);
            }

            return filteredChildren;
        }
        catch (err)
        {
            err.customMessage = `could not getElementImmediateChildrenByTag`;
            throw err;
        }
    }

    // select related functions
    async selectText(xpath, text)
    {
        try
        {
            const element = await this.getElement(xpath);
            await this.selectElementText(element, text);
        }
        catch (err)
        {
            err.customMessage = `could not selectText`;
            throw err;
        }
    }
    async selectElementText(element, text)
    {
        try
        {
            if (element)
            {
                const options = await element.findElements(By.tagName(`option`));
                for (const option of options)
                {
                    const optionValue = await option.getText();
                    if (optionValue === text)
                    {
                        await this.click(option);
                        return;
                    }
                }
            }
        }
        catch (err)
        {
            err.customMessage = `could not selectElementText`;
            throw err;
        }
    }

    async selectValue(xpath, value)
    {
        try
        {
            const element = await this.getElement(xpath);
            await this.selectElementValue(element, value);
        }
        catch (err)
        {
            err.customMessage = `could not selectValue`;
            throw err;
        }
    }
    async selectElementValue(element, value)
    {
        try
        {
            if (element)
            {
                const options = await element.findElements(By.tagName(`option`));
                for (const option of options)
                {
                    const optionValue = await option.getAttribute(`value`);
                    if (optionValue === value)
                    {
                        await this.click(option);
                        return;
                    }
                }
            }
        }
        catch (err)
        {
            err.customMessage = `could not selectElementValue`;
            throw err;
        }
    }

    async selectIndex(xpath, index)
    {
        try
        {
            const element = await this.getElement(xpath);
            await this.selectElementIndex(element, index);
        }
        catch (err)
        {
            err.customMessage = `could not selectIndex`;
            throw err;
        }
    }
    async selectElementIndex(element, index)
    {
        try
        {
            if (element)
            {
                const options = await element.findElements(By.tagName(`option`));
                for (let i = 0; i < options.length; i++)
                {
                    if (i === index)
                    {
                        await this.click(options[i]);
                        return;
                    }
                }
            }
        }
        catch (err)
        {
            err.customMessage = `could not selectElementIndex`;
            throw err;
        }
    }
    // select related functions END
}