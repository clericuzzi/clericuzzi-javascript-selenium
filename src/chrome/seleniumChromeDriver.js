const chrome = require('selenium-webdriver/chrome');
const { Builder } = require('selenium-webdriver');
const SeleniumDriver = require('../base/seleniumDriver');

module.exports = class SeleniumChromeDriver extends SeleniumDriver
{
    constructor(downloadPath, headless = false, logLevel = `--log-level=3`)
    {
        const prefs = {
            "chrome.switches": `–disable-extensions`,
            "plugins.plugins_disabled": [`Chrome PDF Viewer`],
            "download.default_directory": downloadPath,
            "headless": headless,
            "plugins.always_open_pdf_externally": true,
        };
        const options = new chrome.Options();
        if (headless)
            options.addArguments(['--test-type', '--start-maximized', '--disable-popup-blocking', logLevel, '--headless']);
        else
            options.addArguments(['--test-type', '--start-maximized', '--disable-popup-blocking', logLevel]);
        options.setUserPreferences(prefs);

        const builder = new Builder().forBrowser('chrome').setChromeOptions(options);
        super(builder);
    }
}